//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"🌍 ", "conn",						30,		0},
	{"🌡️ ", "temp",						15,		0},
	{"⚡ ", "battery",					60,		0},
	{"☀️ ",  "brightness",					0,		11},
	{"🔊 ", "volume",					0,		10},
	{"", "date '+%a, %d %b %Y, %H:%M %Z'",			30,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = ' ';
